### `composer require infotechnohelp/package-loader'


## Basic usage

Assume that you have `webroot/node_modules` with `jquery` installed

```php
use PackageLoader\PackageLoader;

$npm = new PackageLoader(\Cake\Routing\Router::url('/') . 'node_modules/');

echo $npm->script('jquery/dist/jquery.min');
```


## Basic usage with map

```php
use PackageLoader\PackageLoader;

$npm = new PackageLoader(\Cake\Routing\Router::url('/') . 'node_modules/', CONFIG . 'npm-map');

echo $npm->map()->scripts();
```

Map structure

`config/npm-map.json`

```json
{
  "js": {
    "jquery": "jquery/dist/jquery.min",
    "api-client": "@infotechnohelp/api-client/src/index"
  },
  "css": {
    ...
  }
}
```


Add all scripts

```php
echo $npm->map()->scripts();
```

Add provided scripts only
```php
echo $npm->map()->scripts(['jquery']);
```

Add all except provided scripts
```php
echo $npm->map()->scripts(['jquery'], true);
```


## Sub-packages

Map structure

```json
{
  "js": {
    "jquery": "jquery/dist/jquery.min",
    "auth-api": {
      "login": "cakephp-auth-api-client/src/login",
      "logout" : "cakephp-auth-api-client/src/logout"
    }
  }
}
```

Add all scripts

```php
echo $npm->map()->scripts();
```

Add provided scripts only
```php
echo $npm->map()->scripts(['jquery', 'auth-api:login']);
```

Add all except provided scripts
```php
echo $npm->map()->scripts(['auth-api:logout'], true);
```

(Results will be same, only `jquery` and `login` scripts loaded)


## Load stylesheets

Map structure


```json
{
  "css": {
    ...
  }
}
```

Add a certain stylesheet

```php
echo $npm->stylesheet('path-inside-node_modules');
```

Add all stylesheets

```php
echo $npm->map()->stylesheets();
```

`Everything works same as with scripts (only & except conditions)`