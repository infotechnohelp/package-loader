<?php

namespace PackageLoader;

/**
 * Class PackageLoader
 * @package PackageLoader
 */
class PackageLoader
{
    /**
     * @var null|string
     */
    private $packageVendor = null;

    /**
     * @var null|string
     */
    private $mapConfigPath = null;

    /**
     * @var null | \PackageLoader\PackageLoaderMap
     */
    private $packageMap = null;

    /**
     * PackageLoader constructor.
     *
     * @param string      $packageVendor
     * @param string|null $mapConfigPath
     */
    public function __construct(string $packageVendor, string $mapConfigPath = null)
    {
        $this->packageVendor = $packageVendor;
        $this->mapConfigPath = $mapConfigPath;
    }

    /**
     * @return \PackageLoader\PackageLoaderMap|null
     * @throws \Exception
     */
    public function map()
    {
        if (empty($this->packageMap) && ! empty($this->mapConfigPath)) {
            $this->packageMap = new PackageLoaderMap($this->packageVendor, $this->mapConfigPath);
        }

        if (empty($this->packageMap)) {
            throw new \Exception('Package map is not specified');
        }

        return $this->packageMap;
    }

    /**
     * @param string $fullPath
     *
     * @return string
     */
    public function script(string $fullPath)
    {
        return sprintf(
            '<script type="text/javascript" src="%s%s.js"></script>',
            $this->packageVendor,
            $fullPath
        );
    }

    /**
     * @param string $fullPath
     *
     * @return string
     */
    public function stylesheet(string $fullPath)
    {
        return sprintf(
            '<link rel="stylesheet" type="text/css" href="%s%s.css"/>',
            $this->packageVendor,
            $fullPath
        );
    }
}
